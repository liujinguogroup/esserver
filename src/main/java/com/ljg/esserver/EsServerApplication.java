package com.ljg.esserver;

import com.ljg.common.annotation.EnableEnumMapper;
import com.ljg.common.annotation.EnableGlobalException;
import com.ljg.common.annotation.EnableSwaggerPlugin;
import com.ljg.common.mybatisplus.EnableCustomMyBatisPlusConfig;
//import com.moac.commonseata.ribbon.EnabledCustomRibbonConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;


@EnableGlobalException
@EnableCustomMyBatisPlusConfig
@EnableEnumMapper
@EnableSwaggerPlugin
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.ljg"})
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ComponentScan(basePackages = {"com.ljg"})
public class EsServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(EsServerApplication.class, args);
    }


}
