package com.ljg.esserver.api;


import com.ljg.esserver.models.GoodsSearch;
import com.ljg.esserver.pojo.GoodsTblES;
import com.ljg.esserver.services.GoodsRepository;
import com.ljg.common.utils.response.ResponsePageResult;
import com.ljg.common.utils.response.ResponseResult;
import io.swagger.annotations.Api;
import org.elasticsearch.index.query.FuzzyQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@Api
@RestController
public class GoodsController {

    @Autowired
    private GoodsRepository goodsRepository;

    @PostMapping("/goodsDoc/save")
    public ResponseResult<String> saveGoodsDoc(@RequestBody GoodsTblES dto){
        return ResponseResult.success(goodsRepository.save(dto).toString());
    }

    @PostMapping("/goodsDoc/search")
    public ResponsePageResult<GoodsTblES> search(@RequestBody GoodsSearch search){
        FuzzyQueryBuilder fuzzyQueryBuilder = QueryBuilders.fuzzyQuery("goodsName",search.getGoodsName());
        Pageable pageable = PageRequest.of(search.getPageindex(), search.getPagesize(), Sort.Direction.DESC,"id");
        Page<GoodsTblES> search1 = goodsRepository.search(fuzzyQueryBuilder, pageable);
        return ResponsePageResult.success(search1.getTotalElements(),search1.toList());
    }

}
