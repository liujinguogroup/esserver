package com.ljg.esserver.pojo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.math.BigDecimal;

@Data
@Document(indexName = "goods", shards = 10, replicas = 0)
public class GoodsTblES {

    /**
     * 主键
     * */
    @Id
    private Long id;

    /**
     * 商品名称
     * */
    @ApiModelProperty("商品名称")
    @Field(type = FieldType.Text)
    private String goodsName;

    /**
     * 价格
     * */
    @ApiModelProperty("价格")
    @Field(type = FieldType.Text)
    private BigDecimal price;

    /**
     * 商品主图
     * */
    @ApiModelProperty("商品主图")
    @Field(type = FieldType.Text)
    private String goodsImage;

    /**
     * 商品详情
     * */
    @ApiModelProperty("商品详情")
    @Field(type = FieldType.Text)
    private String goodsDetail;
}
