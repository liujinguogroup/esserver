package com.ljg.esserver.services;

import com.ljg.esserver.pojo.GoodsTblES;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface GoodsRepository extends ElasticsearchRepository<GoodsTblES, Long> {
}
