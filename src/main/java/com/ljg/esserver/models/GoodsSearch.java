package com.ljg.esserver.models;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GoodsSearch extends PageBase{

    private Long id;

    @ApiModelProperty("商品名称")
    private String goodsName;

    @ApiModelProperty("价格范围查询 20_30")
    private String price;

    @ApiModelProperty("商品详情")
    private String goodsDetail;
}
