package com.ljg.esserver.models;

import com.ljg.common.utils.entities.PageEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class PageBase {

    @ApiModelProperty("查询页码")
    private int pageindex;

    @ApiModelProperty("分页大小，即每页数据条数")
    private int pagesize;

    public PageBase() {
    }

    public PageBase(int pageindex, int pagesize) {
        this.pageindex = pageindex;
        this.pagesize = pagesize;
    }

    public PageEntity toPageEntity(){
        return PageEntity.builder().pageindex(pageindex).pagesize(pagesize).build();
    }

}
